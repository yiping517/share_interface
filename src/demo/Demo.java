package demo;

public class Demo {

	public static void main(String[] args) {
		Inter6 o1 = new MM();//繼承有傳遞性
		Inter5 o2 = new MM();
	}
}

//演示接口語法
interface Inter1{
	public static final int NUM = 5;
	public abstract void show();
	int COUNT = 6;//默認public static final
	void test();//默認public abstract
	//int NUMBER ;
	//void say() {};
} 

interface Inter2{
	void show();
	void say();
}
//演示接口的實現
class Ioo implements Inter2{

	@Override
	//void show() {
	public void show() {
	}

	@Override
	public void say() {
	}
}

abstract class KK{
	abstract void uu();
}
interface Inter3{
	void show();
}
interface Inter4{
	void test();
}
//演示繼承、實現
class Koo extends KK implements Inter3, Inter4{

	@Override
	public void test() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void show() {
		// TODO Auto-generated method stub
		
	}

	
	//不加public也行
	@Override
	void uu() {
	}
}

//接口繼承街口
interface Inter5{
	void sho();
}
interface Inter6 extends Inter5{
	void tes();
}
class MM implements Inter6{

	@Override
	public void sho() {
	}

	@Override
	public void tes() {
	}
}